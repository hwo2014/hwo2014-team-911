using System;
using System.IO;
using System.Net.Sockets;
using System.Collections.Generic;
using Newtonsoft.Json;

public class Bot {
	public static void Main(string[] args) {
	
	    Console.WriteLine(string.Format("args: {0:}",args));
	
		string host = args[0];
		int port = int.Parse(args[1]);
		// CI
		
		string botName = "happyt";
		string botKey = "vww6UKx8hAGkWQ";
		Console.WriteLine("v1.24 Connecting to " + host + ":" + port + " as " + botName);
		
		///// online           
		/*
            string botName = args[2];
            string botKey = args[3];
            string raceTrack = args[4];            
            Console.WriteLine("v1.24 Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);
*////
		using ( TcpClient client = new TcpClient(host, port))
		{
			NetworkStream stream = client.GetStream();
			StreamReader reader = new StreamReader(stream);
			StreamWriter writer = new StreamWriter(stream);
			writer.AutoFlush = true;
			
			new Bot(reader, writer, new Join(botName, botKey));
			//                new Bot(reader, writer, new JoinRace(botName, botKey, raceTrack));
		}
	}

	Track TT = new Track();
	Timings TG = new Timings();

	private StreamWriter writer;

	Bot(StreamReader reader, StreamWriter writer, Join join)
	{
		string s = "";
		int count = 0;
		
		
		double throttle = 1.0; 
		double speedDistance = 0;
		int currentPiece = 0;
		double currentOffset = 0;
		int lastPiece = 0;
		double lastOffset = 0;
		double maxAngle = 0;
		int maxLaneNo = 0;
		int currentTrack = 0;       // set when I ask for a change, may be different to currentLane from system
		int currentLane = 0;
		
		double raceMultiplier = 0.5;    // CI value
		int reportTicks = 60;
		int loggingDetail = 2;
		int definition = 3;
		
		//            double raceMultiplier = 1.47;      // france 
		//            double raceMultiplier = 1.25;    // 1.95;      // usa 
		//            double raceMultiplier = 1.8;      // finland 
		//            double raceMultiplier = 1.35;     // germany

        List<Target> targets = new List<Target>();
		List<Section> sections = new List<Section>();
		int currentSegment;
		string filename = "";
		double speedTarget = 0;
		double sectionLength = 77.0;
		double acc = 1.0;
		
		bool switchTaken = false;
		
		this.writer = writer;
		string line;
		
		send(join);
		
	//	System.IO.StreamWriter logFile = new System.IO.StreamWriter("data.csv");
		
		while ((line = reader.ReadLine()) != null)
		{
			MsgWrapper msg = JsonConvert.DeserializeObject<MsgWrapper>(line);
			//                Console.WriteLine("Message: " + msg.msgType);
			switch (msg.msgType)
			{
				case "carPositions":
				
					//      CarPositions cp = JsonConvert.DeserializeObject<CarPositions>(dcp); 
					
					s = "{\"data\":" + msg.data.ToString() + "}";
					CarLocations cloc = JsonConvert.DeserializeObject<CarLocations>(s);
					
					currentPiece = cloc.data[0].piecePosition.pieceIndex;
					currentOffset = cloc.data[0].piecePosition.inPieceDistance;
					currentLane = cloc.data[0].piecePosition.lane.endLaneIndex;
					
					// if different to last piece, distanceMoved =  lastPieceLength - lastOffset + offset in this piece
					// else distanceMoved = thisOffset - lastOffset
					
					if (currentPiece == lastPiece) {
						speedDistance = currentOffset - lastOffset;
					} else {
						speedDistance = pieceLength(lastPiece, currentLane) - lastOffset + currentOffset;
					}
					
					lastOffset = currentOffset;
					lastPiece = currentPiece;
					
					sectionLength = pieceLength(currentPiece, currentLane)/definition;
					currentSegment = currentPiece * definition + (int)(currentOffset / sectionLength);
					
					//// save
					//// check actual speed to see if better than tSpeed
					//targets[currentSegment].lanes[currentLane].speed = speedDistance;
					//// haven't crashed so increase target
					//if (speedDistance > targets[currentSegment].lanes[currentLane].tSpeed)
					//{
					//    targets[currentSegment].lanes[currentLane].tSpeed = speedDistance;
					//}
					
					//// check angle if need to increase
					//// may be a crossing point, so angle should be close to zero
					//targets[currentSegment].lanes[currentLane].angle = cloc.data[0].angle;
					//if (cloc.data[0].angle > targets[currentSegment].lanes[currentLane].tAngle)
					//{
					//    targets[currentSegment].lanes[currentLane].tAngle = cloc.data[0].angle;
					//}
					
					
					// look for target speed in next position
					if ((currentSegment + 1) == targets.Count)
					{
						speedTarget = targets[0].lanes[currentLane].tSpeed;
					}
					else
					{
						speedTarget = targets[currentSegment + 1].lanes[currentLane].tSpeed;
					}
					
					if (speedDistance > 0)    // sometimes get same message twice
					{
						acc = workOutAcceleration(speedTarget - speedDistance);
					} else {
						acc = 0.5;
					}
					
					targets[currentSegment].lanes[currentLane].acceleration = acc;
					
					// haven't crashed so increase target speed
					
					if (cloc.data[0].angle < maxAngle * 0.75) speedTarget *= 1.1;
					if (cloc.data[0].angle > maxAngle * 0.75) speedTarget *= 0.9;
					
					targets[currentSegment].lanes[currentLane].tSpeed = speedTarget;
					
					// Put this speed target in all other places with a similar ratio............?????????
					
					
					
					
					//if (speedDistance < 0)
					//{
					//    ;
					//}
					
					if (speedTarget > 20)
					{
						;
					}
					
					// if slowing down, do less abruptly
					//if (throttle > throttleTarget)
					//{
					//    throttle = (throttle + throttleTarget) / 2.0;
					//}
					//else
					//{
					throttle = targets[currentSegment].lanes[currentLane].acceleration;
					//}
					
					if (cloc.data[0].angle > maxAngle) maxAngle = cloc.data[0].angle;
					
					bool sw = pieceSwitchable(currentPiece + 1);
					int dirn = pieceDirection(currentPiece + 3);
					
					if (!sw) switchTaken = false;  // reset til next one
					
					// if send a SwitchLane, don't update throttle ...?
					bool didSwitch = false;
					
					if (sw)
					{
						;
					}
					
					if (dirn < 0)
					{
						if (sw && currentTrack > 0 && !switchTaken)
						{
							currentTrack -= 1;
							send(new SwitchLane("Left"));
							didSwitch = true;
							switchTaken = true;
						}
					}
					else if (dirn > 0)
					{
						if (sw && currentTrack < maxLaneNo && !switchTaken)
						{
							currentTrack += 1;
							send(new SwitchLane("Right"));
							didSwitch = true;
							switchTaken = true;
						}
					}
					if (!didSwitch) send(new Throttle(throttle));
					//       send(new Throttle(throttle));
					
					count++;
					if (count == reportTicks)
					{
						count = 0;
						string log = string.Format("{0:0}-{1:0}+{2:0.00}, rat={3:0.00}, idx={4:0}, tSpd={5:0.00}, acc={6:0.00}, spd={7:0.00}, ang={8:0.00}, tr:{9:0}:{10:0}",
												   currentSegment,
												   cloc.data[0].piecePosition.pieceIndex,
												   cloc.data[0].piecePosition.inPieceDistance,
												   targets[currentSegment].lanes[currentLane].ratio,           // radius / angle
												   targets[currentSegment].lanes[currentLane].index,           // corner type
												   targets[currentSegment].lanes[currentLane].tSpeed,
												   targets[currentSegment].lanes[currentLane].acceleration,
												   speedDistance,
												   cloc.data[0].angle,
												   currentTrack,
												   currentLane
												  );
						if (loggingDetail > 1)
						{
	//						Console.WriteLine(log);
						}
						
	//					logFile.WriteLine(log);
						
					}
					
					
					break; 
				
				case "join":
					Console.WriteLine("Joined");
					send(new Ping());
					break;
					
				case "gameInit":
					Console.WriteLine("Race init");
					//    send(new Ping());
					
					//                    using (StreamWriter outwriter =
					//new StreamWriter("racetrack.txt"))
					//                    {
					//                        outwriter.WriteLine(msg.data.ToString());
					//                        outwriter.Close();
					//                    }
					
					
					s = "{\"race\":" + msg.data.ToString() + "}";
					Data raceData = JsonConvert.DeserializeObject<Data>(msg.data.ToString());
					
					Console.WriteLine("Track: " + raceData.race.track.name);
					
					// initialize the file objects
					
					
					//      Race race = JsonConvert.DeserializeObject<Race>(dd.race);     // why not?
					
					//var jsonData = "{\"name\":[{\"last\":\"Smith\"},{\"last\":\"Doe\"}]}";
					//NameList myNames = JsonConvert.DeserializeObject<NameList>(jsonData);
					
					
					//                            s = "{\"pieces\":" + dd.race.track.pieces.ToString() + "}";
					//                            PieceCollection tp = JsonConvert.DeserializeObject<PieceCollection>(s);
					////      PieceCollection tp = raceData.race.track;
					TT.pieces = raceData.race.track.pieces;
					Console.WriteLine("Track pieces: " + TT.pieces.Count.ToString());
					
					////           s = "{\"lanes\":" + dd.race.track.lanes.ToString() + "}";
					////           LaneCollection la = JsonConvert.DeserializeObject<LaneCollection>(s);
					
					TT.lanes = raceData.race.track.lanes;
					Console.WriteLine("Track lanes: " + TT.lanes.Count.ToString());
					
					double xx = raceData.race.track.startingPoint.position.x;
					double yy = raceData.race.track.startingPoint.position.y;
					double startAngle = raceData.race.track.startingPoint.angle;
					
					//s = "{\"cars\":" + dd.race.cars.ToString() + "}";
					//CarCollection ca = JsonConvert.DeserializeObject<CarCollection>(s);
					
					//Console.WriteLine("Cars: " + ca.cars.Count.ToString());
					
					//s = "{\"cars\":" + dd.race.track.startingPoint.position.ToString() + "}";
					//Position sp = JsonConvert.DeserializeObject<Position>(s);
					
					TT.name = raceData.race.track.name;
					TT.startingPoint = new StartingPoint();
					TT.startingPoint.position = raceData.race.track.startingPoint.position;
					TT.startingPoint.angle = raceData.race.track.startingPoint.angle;
					
					maxLaneNo = TT.lanes.Count - 1;
					
					// ================= FINISHED READING TRACK
					
					bool readPrevious = false;
					
					filename = "./" + TT.name + ".json";
					if (readPrevious && File.Exists(filename))
					{
						// read in the previous version
						
						Console.WriteLine("Reading..." + filename);
						
						// deserialize JSON directly from a file
						using (StreamReader file = File.OpenText(filename))
						{
							JsonSerializer serializer = new JsonSerializer();
							TG = (Timings)serializer.Deserialize(file, typeof(Timings));
						}
						
						foreach (Target t in TG.aPoints)
						{
							targets.Add(t);
						}
					}
					else
					{
						
						TG.trackName = TT.name;
						TG.multiplier = raceMultiplier;
						TG.definition = definition;
						TG.reportTicks = reportTicks;
						
						double radius = 0;
						int curveIndex = 0;
						
						// calculate starting targets
						for (int i = 0; i < raceData.race.track.pieces.Count; i++)
						{
							for (int d = 0; d < definition; d++)
							{
								// each part of the piece
								Target tg = new Target();
								
								tg.pieceNo = i;
								tg.radius = pieceRadius(i); // or zero
								tg.angle = pieceAngle(i);
								tg.direction = pieceDirection(i);
								tg.switchable = pieceSwitchable(i);
								tg.lanes = new List<TLane>();
								
								//==  calculated
								foreach (Lane LL in TT.lanes)
								{
									TLane tl = new TLane();
									radius = pieceRadius(i);
									if (radius == 0)
									{
										tl.ratio = 0;
									}
									else
									{
										tl.ratio = curveRatio(radius + LL.distanceFromCenter, pieceAngle(i));
									}
									
									tl.length = pieceLength(i, LL.index);
									tl.speed = 0.0;
									tl.angle = 0.0;
									tl.acceleration = 0.5;
									tl.tSpeed = chooseTargetSpeed(i, out curveIndex) * raceMultiplier;
									tl.index = curveIndex;
									tl.tAngle = 60.0;
									tl.maxSpeed = 3.0;      // max with no crash
									tl.crashSpeed = 99.0;   // min having crashed
									
									//                           Console.WriteLine(string.Format("{0:0}: {1:0.00}, {2:0.00}, {3:0.00}, {4:0}", i, radius, pieceAngle(i), tl.ratio, curveIndex));
									
									tg.lanes.Add(tl);
								}
								
								targets.Add(tg);
								
								TG.aPoints.Add(tg);
								
							}
						}
						
						// serialize JSON to a string and then write string to a file
						string filedump = "./" + TT.name + "-track.json";
	//					File.WriteAllText(filedump, JsonConvert.SerializeObject(TG));
						
					}
					
					Console.WriteLine("Targets count: " + targets.Count.ToString());
					
					
					// create sections
					int pp = 0;
					
					Section ss = new Section();
					ss.startPiece = pp;
					double angle = pieceAngle(pp) * pieceDirection(pp);
					double[] dist = { 0, 0, 0, 0 };
					//dist[0] += pieceLength(pp, 0);
					//dist[1] += pieceLength(pp, 1);
					//dist[2] += pieceLength(pp, 2);
					//dist[3] += pieceLength(pp, 3);
					
					while (pp < raceData.race.track.pieces.Count)
					{
						pp++;
						angle += pieceAngle(pp) * pieceDirection(pp);
						dist[0] += pieceLength(pp, 0);
						dist[1] += pieceLength(pp, 1);
						dist[2] += pieceLength(pp, 2);
						dist[3] += pieceLength(pp, 3);
						if (pieceSwitchable(pp))
						{
							ss.endPiece = pp;
							ss.direction = Math.Sign(angle);
							sections.Add(ss);
							ss = new Section();
							ss.startPiece = pp;
							angle = pieceAngle(pp) * pieceDirection(pp);
							dist[0] = pieceLength(pp, 0);
							dist[1] = pieceLength(pp, 1);
							dist[2] = pieceLength(pp, 2);
							dist[3] = pieceLength(pp, 3);
						}
					}
					ss.endPiece = pp;
					ss.direction = Math.Sign(angle);
					sections.Add(ss);
					
					Console.WriteLine("Calculation done");
					send(new Ping());
					
				break;
				
				case "gameEnd":
					EndData raceResult = JsonConvert.DeserializeObject<EndData>(msg.data.ToString());
					
		//			logFile.WriteLine(msg.data.ToString());
					
					double raceTime = raceResult.results[0].result.millis;
					long ticks = raceResult.results[0].result.ticks;
					Console.WriteLine(string.Format("Race ended - ave: {0:0.00}secs, {1:0} ticks, maxAng={2:0.00}", raceTime/3000.0, ticks, maxAngle));
					//       Console.WriteLine("Race ended ");
	//				Console.ReadLine();                        
	//				logFile.Close();
					
					// serialize JSON to a string and then write string to a file
					File.WriteAllText(filename, JsonConvert.SerializeObject(TG));
					
					send(new Ping());
					break;
				case "gameStart":
					Console.WriteLine("Race starts");
					
					send(new Ping());
					break;
					case "lapFinished":
					send(new Ping());
					LapData lapData = JsonConvert.DeserializeObject<LapData>(msg.data.ToString());
					
					long time = lapData.lapTime.millis;
					string raceCar = (lapData.raceCar != null) ? lapData.raceCar.name : "???";
					Console.WriteLine(string.Format("Lap, Car({0:}: {1:0.000}", raceCar,time/1000.0));
					
					
					break;
					
				case "crash":
					Console.WriteLine("\nCrashed\n");
					send(new Ping());
					break;
					
				default:
					Console.WriteLine("==== Unhandled: " + msg.msgType);
					Console.WriteLine("==== Unhandled data: " + msg.data);
					send(new Ping());
				break;
			}
		}
	}


	private void send(SendMsg msg) {
		writer.WriteLine(msg.ToJson());
	}
    
	/// <summary>
	/// Enter increase in speed required
	/// </summary>
	/// <param name="p"></param>
	/// <returns>Acceleration needed or max/min</returns>
	private double workOutAcceleration(double speedDelta)
	{
		double acc;
		double midPoint = 0.4;
		acc = midPoint + (speedDelta / 5);
		if (acc > 0.99) acc = 0.99;
		if (acc < 0) acc = 0.0;
		return acc;
	}
	
	/// <summary>
	/// Calculates position around the track based on standard length
	/// </summary>
	/// <param name="trackUnit"></param>
	/// <returns></returns>
	private double lapPosition(int trackUnit)
	{
		double position = 0;
		// for first piece
		if (trackUnit == 0) return 0;
		
		//           Console.Write("length...");
		for (int i = 0; i < trackUnit; i++)
		{
			if (TT.pieces[i].radius != null)
			{
				// curved position
				int radius = (TT.pieces[i].radius == null) ? 0 : (int)TT.pieces[i].radius;
				// adjust for track
				radius += 10;
				double angle = (TT.pieces[i].angle == null) ? 0 : (double)TT.pieces[i].angle;
				angle = Math.Abs(angle);
				double curve = (radius * Math.PI * angle) / 180.0;
				position += curve;
				
				if (curve == 0) Console.WriteLine(string.Format("Zero: radius/angle...{0:0.00},{1:0.00}", TT.pieces[i].radius, TT.pieces[i].angle));
				//Console.WriteLine(string.Format("radius/angle/length...{0:0}, {1:0.00}, {2:00} ",
				//    TT.pieces[i].radius,
				//    TT.pieces[i].angle,
				//    curve));
			}
			else
			{
				// straight
				position += TT.pieces[i].length;
			}
			//                 Console.Write(string.Format(".{0:0}", position));
		}
		//            Console.WriteLine("");
		return position;
	}
	/// <summary>
	/// Get piece length for this lane
	/// </summary>
	/// <param name="n"></param>
	/// <param name="track"></param>
	/// <returns>Piece length calculated</returns>
	private double pieceLength(int n, int lane)
	{
		double theLength = 0;
		int pieceNo = n;
		int track = lane;
		// if > no of lanes
		if (track >= TT.lanes.Count) track = TT.lanes.Count - 1;
		
		// if over end of lap
		if (pieceNo >= TT.pieces.Count) pieceNo -= TT.pieces.Count;
		
		if (TT.pieces[pieceNo].radius != null)
		{
			// curved position
			int radius = (TT.pieces[pieceNo].radius == null) ? 0 : (int)TT.pieces[pieceNo].radius;
			
			double angle = (TT.pieces[pieceNo].angle == null) ? 0 : (double)TT.pieces[pieceNo].angle;
			// adjust for track
			if (angle < 0)
			{
				radius -= TT.lanes[track].distanceFromCenter;
			}
			else
			{
				radius += TT.lanes[track].distanceFromCenter;
			}
			angle = Math.Abs(angle);
			theLength = (radius * Math.PI * angle) / 180.0;
		}
		else
		{
			// straight
			theLength = TT.pieces[pieceNo].length;
		}
		return theLength;
	}
	
	/// <summary>
	/// Gives the radius or zero for straight
	/// </summary>
	/// <param name="n"></param>
	/// <returns></returns>        
	private double pieceRadius(int n)
	{
		int pieceNo = n;
		// if over end of lap
		if (pieceNo >= TT.pieces.Count) pieceNo -= TT.pieces.Count;
		if (TT.pieces[pieceNo].radius != null)
		{
			// curve
			return (double)TT.pieces[pieceNo].radius;
		}
		else
		{
			return 0;
		}
	}
	
	/// <summary>
	/// Judge which way the bend is, so we can switch to inside
	/// </summary>
	/// <param name="n"></param>
	/// <returns>-1 left, 0 for straight, +1 for right ?? or opposite way round!</returns>
	private int pieceDirection(int n)
	{
		int pieceNo = n;
		// if over end of lap
		if (pieceNo >= TT.pieces.Count) pieceNo -= TT.pieces.Count;
		if (TT.pieces[pieceNo].radius != null)
		{
			// curve
			return Math.Sign((int)TT.pieces[pieceNo].angle);
		}
		else
		{
			return 0;
		}
	}
	
	public int tightness(double radius, double angle)
	{
		double tt;
		if (radius == 0)
		{
			tt = 10;
		}
		else
		{
			tt = radius / angle;
		}
		
		// 100, 45 -> 2...
		//  50,  45 -> 1...
		//  50, 22.5 -> 2...
		//  100, 22.5 -> 4....
		// 100, 45  -> 2.2...
		//  200, 45  -> 4...
		// 200, 22  -> 8.5...
		if (tt > 8)
		{
			return 0;
		}
		else if (tt > 3.5)
		{
			return 1;
		}
		return 2;
	}
	
	public double curveRatio(double radius, double angle)
	{
		double tt;
		if (radius == 0)
		{
			tt = 0;
		}
		else
		{
			tt = radius / angle;
		}
		return tt;
	}
	
	
	public double chooseTargetSpeed(int currentPiece, out int option)
	{
		//                        0,   1,   2,   3,   4,   5    6,   7,   8,   9,   10   11   12  13   14   15   16   17   18   19   20   21   22   23   24   25   26
		//                       aaa, aab, aac, aba, abb, abc, aca, acb, acc, baa, bab, bac, bba, bbb, bbc, bca, bcb, bcc, caa, cab, cac, cba, cbb, cbc, cca, ccb, ccc
		double[] targetSpeed = { 9.0, 6.0, 6.0, 7.5, 4.0, 3.5, 3.0, 3.0, 4.5, 5.0, 6.0, 4.5, 5.5, 5.8, 5.0, 3.6, 3.5, 3.0, 3.0, 4.8, 4.8, 4.0, 4.4, 4.0, 3.5, 3.0, 3.0 };
		
		// Check if on curve, or if coming up
		int a = tightness(pieceRadius(currentPiece), pieceAngle(currentPiece));
		int b = tightness(pieceRadius(currentPiece + 1), pieceAngle(currentPiece + 1));
		int c = tightness(pieceRadius(currentPiece + 2), pieceAngle(currentPiece + 2));
		
		option = a * 9 + b * 3 + c;
		if (option > 26) option = 26;
		
		return targetSpeed[option];
		// TODO scale this?
		
	}
	
	/// <summary>
	/// Get the abs angle of the piece
	/// </summary>
	/// <param name="n"></param>
	/// <returns></returns>
	private double pieceAngle(int n)
	{
		int pieceNo = n;
		// if over end of lap
		if (pieceNo >= TT.pieces.Count) pieceNo -= TT.pieces.Count;
		if (TT.pieces[pieceNo].radius != null)
		{
			// curve
			return Math.Abs((double)TT.pieces[pieceNo].angle);
		}
		else
		{
			return 0;
		}
	}
	
	/// <summary>
	/// Return boolean switchable
	/// </summary>
	/// <param name="n"></param>
	/// <returns></returns>
	private bool pieceSwitchable(int n)
	{
		int pieceNo = n;
		// if over end of lap
		if (pieceNo >= TT.pieces.Count) pieceNo -= TT.pieces.Count;
		bool switchable = (TT.pieces[pieceNo].@switch == null) ? false : (bool)TT.pieces[pieceNo].@switch;
		return switchable;
	}
	
}

class MsgWrapper
{
	public string msgType;
	public Object data;
	
	public MsgWrapper(string msgType, Object data)
	{
		this.msgType = msgType;
		this.data = data;
	}
}

abstract class SendMsg
{
	public string ToJson()
	{
		return JsonConvert.SerializeObject(new MsgWrapper(this.MsgType(), this.MsgData()));
	}
	protected virtual Object MsgData()
	{
		return this;
	}
	
	protected abstract string MsgType();
}

class Join : SendMsg
{
	public string name;
	public string key;
	public string color;
	
	public Join(string name, string key)
	{
		this.name = name;
		this.key = key;
		this.color = "red";
	}
	
	protected override string MsgType()
	{
		return "join";
	}
}

public class BotId
{
	public string name;
	public string key;
}

class JoinRace : SendMsg
{
	public BotId botId;
	public string trackName;
	public string carCount;
	public JoinRace(string name, string key, string location)
	{
		this.botId = new BotId();
		this.botId.name = name;
		this.botId.key = key;
		this.trackName = location;
		this.carCount = "1";
	}
	
	protected override string MsgType()
	{
		return "joinRace";
	}
}

class Ping : SendMsg
{
	protected override string MsgType()
	{
		return "ping";
	}
}

class Throttle : SendMsg
{
	public double value;
	
	public Throttle(double value)
	{
		this.value = value;
	}
	
	protected override Object MsgData()
	{
		return this.value;
	}
	
	protected override string MsgType()
	{
		return "throttle";
	}
}

class SwitchLane : SendMsg
{
	public string value;
	
	public SwitchLane(string value)
	{
		this.value = value;
	}
	
	protected override Object MsgData()
	{
		return this.value;
	}
	
	protected override string MsgType()
	{
		return "switchLane";
	}
}




public class Track
{
	public string id { get; set; }
	public string name { get; set; }
	public List<Piece> pieces { get; set; }
	public List<Lane> lanes { get; set; }
	public StartingPoint startingPoint { get; set; }
}

public class PieceCollection
{
	public List<Piece> pieces { get; set; }
}

public class Piece
{
	public double length { get; set; }
	public bool? @switch { get; set; }
	public int? radius { get; set; }
	public double? angle { get; set; }
}

public class LaneCollection
{
	public List<Lane> lanes { get; set; }
}

public class Lane
{
	public int distanceFromCenter { get; set; }
	public int index { get; set; }
}


public class StartingPoint
{
	public Position position { get; set; }
	public double angle { get; set; }
}

public class Position
{
	public double x { get; set; }
	public double y { get; set; }
}


public class Id
{
	public string name { get; set; }
	public string color { get; set; }
}

public class Dimensions
{
	public double length { get; set; }
	public double width { get; set; }
	public double guideFlagPosition { get; set; }
}

public class CarCollection
{
	public List<Car> cars { get; set; }
}


public class Car
{
	public Id id { get; set; }
	public Dimensions dimensions { get; set; }
}

public class RaceSession
{
	public int laps { get; set; }
	public int maxLapTimeMs { get; set; }
	public bool quickRace { get; set; }
}
public class Race
{
	public Track track { get; set; }
	public List<Car> cars { get; set; }
	public RaceSession raceSession { get; set; }
}

public class Data
{
	public Race race { get; set; }
}

public class LapData
{
	public RaceCar raceCar;
	public RaceLapData lapTime;
	public RaceData raceTime;
	public Ranking ranking;
}

public class EndData
{
	public List<Result> results { get; set; }
	public List<Bestlap> bestLaps { get; set; }
}

public class Ranking
{
	public int overall;
	public int fastestLap;
}

public class Timings
{
	public string trackName = "test";  // will look for this name
	public double multiplier = 1.0;
	public int definition = 3;
	public int reportTicks = 60;
	public List<Target> aPoints = new List<Target>();
}

public class Target
{
	public int pieceNo;
	public double radius;
	public double angle;    // abs value
	public int direction;   // -1, 0, 1
	public bool switchable;
	public List<TLane> lanes;
}

public class TLane
{
	public double ratio;         // radius / degrees
	public int index;            // curve index abc
	public double length;        // length of lane (for whole piece?)
	public double speed;         // speed last time
	public double angle;        // angle last time
	public double acceleration; // acceleration last time
	public double tSpeed;       // target speed
	public double tAngle;       // target slide angle
	public double maxSpeed;     // max speed -> no crash 
	public double crashSpeed;  // min speed -> crash 
}

// pieces between switch positions
public class Section
{
	public int startPiece;
	public int endPiece;
	public int direction; // to show inside track
	
	// for each lane
	public double[] distance = { 0, 0, 0, 0 };
	public double[] time = { 0, 0, 0, 0 };
	public double[] multiplier = { 1, 1, 1, 1 };
	
}

public class Result
{
	public RaceCar car { get; set; }
	public RaceLapData result { get; set; }
}

public class RaceCar
{
	public string name { get; set; }
	public string color { get; set; }
}

public class Bestlap
{
	public RaceCar car { get; set; }
	public RaceLapData result { get; set; }
}

public class RaceLapData
{
	public int lap { get; set; }
	public int ticks { get; set; }
	public int millis { get; set; }
}
public class RaceData
{
	public int laps { get; set; }
	public int ticks { get; set; }
	public int millis { get; set; }
}

public class LaneI
{
	public int startLaneIndex { get; set; }
	public int endLaneIndex { get; set; }
}

public class PiecePosition
{
	public int pieceIndex { get; set; }
	public double inPieceDistance { get; set; }
	public LaneI lane { get; set; }
	public int lap { get; set; }
}

public class CarLocations
{
	public List<CarPosition> data { get; set; }
}

public class CarPosition
{
	public Id id { get; set; }
	public double angle { get; set; }
	public PiecePosition piecePosition { get; set; }
}

public class RootObject
{
	public string msgType { get; set; }
	public List<CarPosition> data { get; set; }
	public string gameId { get; set; }
	public int gameTick { get; set; }
}
